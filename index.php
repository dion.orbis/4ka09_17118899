<!DOCTYPE html>
<html lang="en">
<head>
    <title>Food Talk</title>
    <meta charset="UTF-8">
    <meta name="description" contents="FoodTalk">
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>

    <div class="wrapper">
    <header>
        <h2 class="title"><center><b><font color="#A81C1E">Food Talk</font></b><center></h2>
        <h3 class="desc"><Center><font color="#A81C1E"><i>Your Best Place for Food Review</i></font></Center></h3>
        <nav id="navigation">
            <ul>
                <li><a href="index.php?page=home">Home</a></li>
                <li><a href="index.php?page=daftarmakanan">Daftar Makanan</a></li>
                <li><a href="index.php?page=contact">Contact</a></li>
                <li><a href="index.php?page=about">About</a></li>
                

            </ul>
        </nav>
    </header>

<div class="wrapper">
<input class="search" type="text" placeholder="Cari...">    
<input class="button" type="button" value="Cari">
</div>
    <div id="contents">
        <?php 
        if(isset($_GET['page'])){
            $page = $_GET['page'];
 
            switch ($page) {
                case 'home':
                include "home.php";
                break;
                case 'daftarmakanan';
                include "daftarmakanan.php";
                break;
                case 'contact':
                include "contact.php";
                break;
                case 'about':
                include "about.php";
                break;    
               
            }
        }
else{
            include "home.php";
        }
        ?>
<aside>
    <section class="makanan populer">
      <h2>Makanan yang Populer Sekarang</h2>
      <a href="buburbarito.html">Bubur Ayam Barito</a>
      <a href="sate taichan.html">Sate Taichan "chikin"</a>
      <a href="bebek kaleyo.html">Bebek Kaleyo</a>
      <a href="burger blenger.html">Burger Blenger</a>
      <a href="kwetiau akang.html">Kwetiau Akang</a>
</section>
  
        </aside>
 
    </div>
    <footer>
        &copy Copyright Food Talk 2021 | Your Best Place for Food Review
    </footer>
</div>
</body>
</html>